package no.noroff.MovieCharacterAPI.repositories;

import no.noroff.MovieCharacterAPI.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Long> {
}
