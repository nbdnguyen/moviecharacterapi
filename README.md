# Movie Character API

A datastore and interface to store and manipulate movie  characters. 

## Tech stack
- Spring
    - Spring Data JPA, Hibernate
    - Spring Boot DevTools
    - Spring Web
    - PostgreSQL Driver

## Includes
- Database
- REST API 
- /src/main/java/no/noroff/MovieCharacterApi
    - MovieCharacterApiCollection: runs the program
    - /controllers
        - CharacterController (RestController) at /api/v1/characters
            - /: GET Displays all characters, POST adds a new character
            - /\{id} : GET Displays a specific character, PUT updates this character, DELETE deletes this character
        - FranchiseController (RestController) at /api/v1/franchises
            - /: GET displays all franchises, POST adds a new franchise
            - /\{id}: GET displays a specific franchise, PUT updates this specific franchise, DELETE deletes this franchise
            - /\{id}/movies: GET displays all the movies in a specific franchise
            - /\{id}/characters: GET displays all characters in all the movies in this specific franchise
        - MovieController (RestController) at /api/v1/movies
            - /: GET displays all movies, POST adds a new movie, 
            - /\{id}: GET displays a specific movie, PUT update this movie, DELETE deletes this movie
            - /\{id}/characters: GET displays all the characters in a specific movie
    - /models
        - Character: id, alias, fullName, gender, picture, movies
        - Franchise: id, name, description, movies
        - Movie: id, title, genre, releaseYear, director, picture, trailer
    - /repositories
        - CharacterRepository (interface)
        - FranchiseRepository (interface)
        - MovieRepository (interface)

#### Made by
- Simenhr: Simen Røstum
- Carlheimdal: Carl Heimdal
- Nbdnguyen: Nghi Nguyen